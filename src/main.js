/* eslint-disable no-unused-vars */
import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';

// Vue Router
import router from './router'

Vue.config.productionTip = false

const vs = new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
