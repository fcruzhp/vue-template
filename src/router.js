/* eslint-disable no-unused-vars */
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior () {
      return { x: 0, y: 0 }
    },
    routes: [
        {
            path: '/',
            name: 'Home',
            component: () => import('./views/Home.vue')
          },
          {
            path: '/admin',
            name: 'admin',
            component: () => import('./views/Admin.vue')
          },
          {
            path: '/cadastro',
            name: 'cadastro',
            component: () => import('./views/Cadastro.vue')
          },
    ]
})

export default router